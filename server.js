// call the packages we need
let express    = require('express');        // call express
let app        = express();                 // define our app using express
let bodyParser = require('body-parser');
let mongoose   = require('mongoose');
let usersRouter = require('./route/userRoute.js');
let viewsRouter = require('./route/viewRoute.js');
let cors = require('cors');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

let port = 1234;        // set our port

mongoose.connect('mongodb://yarinyona:yarin123@ds253879.mlab.com:53879/yarinlearndb', (err) => {
    console.log('connected');
    if(err) {
        console.log(err)
    }

}); // connect to our database
require('./config/passport.js')(app);

app.use('/users', usersRouter);
app.use('/api', viewsRouter);
// START THE SERVER
app.listen(port);
console.log('Magic happens on port ' + port);