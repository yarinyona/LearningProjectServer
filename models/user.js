let mongoose     = require('mongoose');
let Schema       = mongoose.Schema;

let UserSchema   = new Schema({
    username: String,
    password: String,
    isAdmin: Boolean,
}, { collection: 'myCollection'});

module.exports = mongoose.model('User', UserSchema);