let View = require('../models/view.js');
var express = require('express');
var router = express.Router();
var passport = require('passport'); 

var SECRET = "AK-47";
var FAIL =  400;
var CREATED = 201;
var OK = 200;

router.use((req, res, next) =>{
    // do logging
    console.log('Something is happening. ' + req.originalUrl);
    next(); // make sure we go to the next routes and don't stop here
});

router.post('/views',passport.authenticate('bearer', { session: false }), //new View
    (req,res)=>{
        let view = new View();
        view.viewName = req.body.viewName;
        view.centerLocation = {x: req.body.centerx, y: req.body.centery};
        view.items = req.body.items;
        view.save(function(err){
            if (err)
                return res.json({status: FAIL, message:err});
            return res.json({ status: CREATED, message: "View Created"});
        });
    })

router.get('/views',(req,res)=>{ // Get all views
    View.find({},(err,views)=>{
        if (err)
            return res.json({status: FAIL, message:err});
        return res.json(views);
    })
})

router.get('/views/:view_id',passport.authenticate('bearer', { session: false }), //edit View
    (req,res)=>{
        View.findById(req.params.view_id, (err, view) => {
            if (err)
                return  res.json({status: FAIL, message:err}); 
            return res.json(view);
        })
    })

router.delete('/views/:view_id',passport.authenticate('bearer', { session: false }), //delete View
    (req,res)=>{
        View.findById(req.params.view_id, (err, view) => {
            if (err)
                return res.json({status: FAIL, message:err}); 
            View.remove({_id: view._id}, function(err,doc){
                if (err) return res.json({status: FAIL, message: err});
                    return res.json({status: OK, message:"view deleted"});
            })
        })
    })
    
module.exports = router;