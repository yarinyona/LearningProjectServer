let User = require('../models/user.js');
var express = require('express');
var router = express.Router();
var passport = require('passport'); 

var SECRET = "AK-47";
var FAIL =  400;
var CREATED = 201;
var OK = 200;

router.use((req, res, next) =>{
    // do logging
    console.log('Something is happening.' + req.originalUrl);
    next(); // make sure we go to the next routes and don't stop here
});

router.post('/add/', // create user
        passport.authenticate('bearer', { session: false }),
        (req, res) =>{
            let user = new User();      // create a new instance of the user model
            user.username = req.body.username;  // set the users details (comes from the request)
            user.password = req.body.password;
            user.isAdmin = req.body.isAdmin;
            // save the user and check for errors
            user.save(function(err){
                if (err)
                    res.send(err);
                res.json({ status: CREATED, message: "User Created"});
        });

    })
router.get('/all', //get all users
    (req, res)=> {
        User.find({}, function(err, users){
            if (err) 
                return res.json(null);
            return res.json(users);
        });
    })
router.post('/login',passport.authenticate('local'),(req, res) =>{  //user login
    var user = req.body;
    var id = req.user._id
    req.login(user, function(err){
        if (err) { return  res.json({status: FAIL, message:err}); }
        user.password = '';
        return res.json({ status: OK , token: id, user:user});
  })
})
router.post('/logout', function(req, res){  //user logout
	req.logout();
    res.json({ status: OK, message: "User Logged Out"});
});
router.get('/:user_id',passport.authenticate('bearer', { session: false }),(req, res) =>{  //get spesific user by id
        User.findById(req.params.user_id, (err, user) => {
            if (err)
                return  res.json({status: FAIL, message:err}); 
            return res.json({ status: OK , user:user});
        });
    })
router.put('/edit/:user_id',passport.authenticate('bearer', { session: false }),(req, res)=> { //edit user
        if (! req.body.isAdmin ) return res.json({status: FAIL, message:"must be admin to edit user"});
        // use our user model to find the user we want
        User.findById(req.params.user_id, (err, user) =>{

            if (err)
                return res.json({status: FAIL, message:"wrong user id"});
            user.username    = req.body.username; // update the users info
            user.password    = req.body.password;
            user.isAdmin     = req.body.isAdmin; 
            // save the user
            user.save((err) =>{
                if (err)
                    return res.json({status: FAIL, message:"failed save "+err});
                return res.json({status: OK, message:"edit saved"});
            });

        });
    })
router.delete('/edit/:user_id',passport.authenticate('bearer', { session: false }), //delete user
        function(req,res) {
            
            if (! req.user.isAdmin ) return res.json({status: FAIL, message:"must be admin to delete user"});

            User.findById(req.params.user_id, function(err, doc){
                if (err) return res.json({status: FAIL, message: err});
                
                User.remove({_id: doc._id}, function(err,doc){
                    if (err) return res.json({status: FAIL, message: err});
                        return res.json({status: OK, message:"user deleted"});
                }); 
            });
        
    });
router.get("/isadmin/:token",  //is admin
function(req, res){
    User.findById(req.params.token, function(err, user){
        if (err) return false
        return res.send(user._doc.isAdmin);
    });
});
    module.exports = router;