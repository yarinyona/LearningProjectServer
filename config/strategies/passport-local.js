var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
let User = require('../../models/user.js');

module.exports = () => {
    passport.use( new LocalStrategy(
        (i_username,i_password,done) =>{
            User.findOne({ username:i_username,password:i_password}, (err,user)=>{
                if (err) 
                    return done("wrong username or password",user);
                return done(err, user);
            })
        }
    ))
}