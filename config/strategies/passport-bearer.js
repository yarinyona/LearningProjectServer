var passport = require('passport')
var BearerStrategy = require('passport-http-bearer').Strategy;
let User = require('../../models/user.js');

module.exports = () => {
    passport.use( new BearerStrategy(
        (token,done) =>{
            User.findOne({ _id:token}, (err,user)=>{
                if (err) 
                    return done(err);
                return done(err, user);
            })
        }
    ))
}